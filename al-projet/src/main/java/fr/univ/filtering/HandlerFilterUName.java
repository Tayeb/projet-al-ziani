package fr.univ.filtering;

import fr.univ.builder.Header;

public class HandlerFilterUName extends HandlerFilter {
	
	public HandlerFilterUName(String value) {
		// TODO Auto-generated constructor stub
		super(value);
	}

	@Override
	public boolean requestFiltering(Header header) {
		// TODO Auto-generated method stub
		if(header.getFileUNAME().trim().equalsIgnoreCase(this.value)) {
			return this.successor.requestFiltering(header);
		}
		return false;
	}

}
