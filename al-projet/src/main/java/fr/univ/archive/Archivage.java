package fr.univ.archive;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import fr.univ.builder.HeaderFileBuilder;
import fr.univ.builder.Director;
import fr.univ.builder.Header;


public class Archivage {
	
	
	private HeaderFileBuilder headerFileBuilder;
	
	private Director director;
	
	private Header header;
 
	private File tarFile;
	
	private File folder;
	
	private DataOutputStream writer;
	
	
	public Archivage(HeaderFileBuilder headerFileBuilder, File folder) throws FileNotFoundException {
		this.folder = folder;
		this.director = new Director(headerFileBuilder);
		this.headerFileBuilder = headerFileBuilder; 
		this.tarFile = new File(this.folder.getAbsolutePath().substring(0, this.folder.getAbsolutePath().lastIndexOf("/") + 1) + this.folder.getName().replaceAll(" ", "_") + ".ztar");
		this.writer = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(this.tarFile)));
	}
	
	public void archive() throws FileNotFoundException, IOException {
		this.archiveImpl(this.folder);
		if (this.writer != null) {
			this.writer.flush();
			this.writer.close();
		}
	}

	private void archiveImpl(File file) throws FileNotFoundException, IOException {
		DataInputStream reader = null;
		this.director.constructHeader(file);
		this.header = this.headerFileBuilder.getHeader();
		this.streamingHeader(this.header, this.writer);
		try {
			File[] files = file.listFiles();
			for (File f : files) {
				if (f.isFile()) {
					this.director.constructHeader(f);
					this.header = this.headerFileBuilder.getHeader();
					this.streamingHeader(this.header, writer);
					reader = new DataInputStream(new FileInputStream(f));
					final byte[] buffer = new byte[1024]; 
					int dataSize;
					while ((dataSize = reader.read(buffer, 0, buffer.length)) != -1) {
						this.writer.write(buffer, 0, dataSize);
					}
					reader.close();
				}else {
					archiveImpl(f);
				}
			}
		} catch (IOException e) {
			System.out.println("Archiving Error");
			e.printStackTrace();
		} finally {
			try {
				
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				System.out.println("Error on close");
				e.printStackTrace();
			}
		}
	}

	public static int calculCheckSum(StringBuffer buffer) {
		int checkSomme = 0;
		byte b;
		for (int i = 0; i < buffer.length(); i++) {
			b = (byte) buffer.charAt(i);
			checkSomme += b * (i + 1);
		}
		checkSomme &= 0xFF;
		return checkSomme;
	}
	
	private void streamingHeader(Header header, DataOutputStream writer) throws IOException {
		writer.writeBytes(this.fillWithWhiteSpace(header.getFileName(), 100));
		writer.writeBytes(header.getFilePermission());
		writer.writeBytes(this.fillWithWhiteSpace(header.getFileUID(), 8));
		writer.writeBytes(this.fillWithWhiteSpace(header.getFileGID(), 8));
		writer.writeBytes(this.fillWithWhiteSpace(header.getFileSIZE() + "", 12));
		writer.writeBytes(header.getFileLastModifiedDate().substring(0, 16).replaceAll("[-|T|:]", ""));
		writer.writeBytes(header.getFileCheckSum());
		writer.writeBytes(header.getFileFlag() + "");
		writer.writeBytes(header.getFileTypeUSTAR());
		writer.writeBytes(header.getFileVersionUSTAR());
		writer.writeBytes(this.fillWithWhiteSpace(header.getFileUNAME(), 32));
		writer.writeBytes(this.fillWithWhiteSpace(header.getFileGNAME(), 32));
		writer.writeBytes(header.getFileDevMAJOR());
		writer.writeBytes(header.getFileDevMINOR());
		writer.writeBytes(header.getFileLinkName());
		writer.writeBytes(this.fillWithWhiteSpace(header.getFilePrefix(), 155));
	}
	
	private String fillWithWhiteSpace(String field, int left) {
		int i = left - field.length();
		for (; i > 0; i--)
			field += " ";
		return field;
	}
}
