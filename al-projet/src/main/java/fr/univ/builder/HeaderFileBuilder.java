package fr.univ.builder;

public interface HeaderFileBuilder {
	
	public void initHeader();
	public void setFileName(String fileName); 					// 100  ***
	public void setFilePermission(String filePermission); 		// 8	***
	public void setFileUID(String fileUID); 					// 8	***
	public void setFileGID(String fileGID);						// 8	***
	public void   setFileSIZE(long fileSize); 					// 12	***
	public void setFileLastModifiedDate(String fileLastModifiedDate); 		// 12	***
	public void setFileCheckSum(String fileCheckSum); 				// 8
	public void   setFileFlag(byte i); 					// 1
	public void setFileLinkName(String fileLinkName); 				// 100	***
	public void setFileTypeUSTAR(String fileTypeUSTAR);				// 5	***
	public void setFileVersionUSTAR(String fileVersionUSTAR);			// 3	***
	public void setFileUNAME(String fileUNAME); 				// 32	***
	public void setFileGNAME(String fileGNAME); 				// 32	***
	public void setFileDevMAJOR(String fileDevMAJOR); 				// 8	***
	public void setFileDevMINOR(String fileDevMINOR); 				// 8	***
	public void setFilePrefix(String filePrefix); 				// 155	***
	public Header getHeader();
}
