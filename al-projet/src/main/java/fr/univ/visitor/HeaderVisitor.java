package fr.univ.visitor;

import java.io.File;

import fr.univ.builder.Header;

public abstract class HeaderVisitor {
	public static String ROOT_PATH;
	
	public abstract void visitHeaderElementDirectory(Header header);
	public abstract void visitHeaderElementFile(Header headerFile);
	
	protected String tabular(String path, String root_path) {
		int length = path.replace(root_path, "").split(File.separator).length;
		String tabular = "";
		for (int i = 0; i < length; i++) {
			tabular += "\t";
		}
		return tabular;
	}
}
