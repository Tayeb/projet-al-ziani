package fr.univ.visitor;

import fr.univ.builder.Header;

public class HeaderVisitorSample extends HeaderVisitor {

	public void visitHeaderElementDirectory(Header headerDirectory) {
		// TODO Auto-generated method stub
		System.out.println(this.tabular(headerDirectory.getFilePrefix().trim(), HeaderVisitorSample.ROOT_PATH) + "[" + headerDirectory.getFileName().trim() + "]");
	}

	public void visitHeaderElementFile(Header headerFile) {
		// TODO Auto-generated method stub
		System.out.println(this.tabular(headerFile.getFilePrefix().trim(), HeaderVisitorSample.ROOT_PATH) + headerFile.getFileName().trim());
	}

}
