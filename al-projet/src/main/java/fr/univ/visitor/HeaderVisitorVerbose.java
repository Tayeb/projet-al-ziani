package fr.univ.visitor;

import fr.univ.builder.Header;

public class HeaderVisitorVerbose extends HeaderVisitor{

	public void visitHeaderElementDirectory(Header headerDirectory) {
		// TODO Auto-generated method stub
		System.out.println(this.tabular(headerDirectory.getFilePrefix().trim(), HeaderVisitorSample.ROOT_PATH) + "[" + headerDirectory.getFileName().trim() + "]" + "  "
		+ "(" + "uid=" + headerDirectory.getFileUNAME().trim() + "(" + headerDirectory.getFileUID().trim()
		+"),gid=" + headerDirectory.getFileGNAME().trim() + "(" + headerDirectory.getFileUID().trim() + "),mode=" + headerDirectory.getFilePermission() + ")");
	}

	public void visitHeaderElementFile(Header headerFile) {
		// TODO Auto-generated method stub
		System.out.println(this.tabular(headerFile.getFilePrefix().trim(), HeaderVisitorSample.ROOT_PATH) + headerFile.getFileName().trim() + "  "
				+ "(size=" + headerFile.getFileSIZE() + ",date=" + headerFile.getFileLastModifiedDate().trim() + ",uid=" + headerFile.getFileUNAME().trim() + "(" + headerFile.getFileUID().trim()
				+"),gid=" + headerFile.getFileGNAME().trim() + "(" + headerFile.getFileUID().trim() + "),mode=" + headerFile.getFilePermission() + ")");
	}

}
