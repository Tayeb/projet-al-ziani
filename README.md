 **Les auteurs du projet sont:**
       - Tayeb ZIANI 
       - Abdelkader MOUEDDEN
       - Pierre SINEGRE
 **L'execution se fait par la commande (se placer dans le dossier racine al-projet)**
        - pour la commande **archive**:
            java -cp target/al-projet-0.0.1-SNAPSHOT.jar fr.univ.app.App archive absolute-path-of-directory
        - pour la commande **list**:
            java -cp target/al-projet-0.0.1-SNAPSHOT.jar fr.univ.app.App list absolute-path-of-archived-file [options]
        - pour la commande **extract**:
            java -cp target/al-projet-0.0.1-SNAPSHOT.jar fr.univ.app.App extract absolute-path-of-archived-file absolute-path-of-target-directory [options]

 **Vous trouvez ci-joint un dossier contient des fichiers de test.**
       
